# Check if the script has been invoked pre-installation or post-installation.
#
# This is done by checking if RAM has been mounted at root. Of course, this will
# fail if a live environment is directly run from the drive it is on. I'm
# looking for suggestions to make this flexible.
detect_mode() {
    if ! mount | grep -qF 'tmpfs on / '; then
        MODE="POST"
    else
        MODE="PRE"
    fi

    export MODE
}

# Read a set of environment variables that aid in determining the machine to
# install on and its hadware configuration. Those are:
# - DISPLAY (The machine name as displayed in the output)
# - MACHINE (The machine name as it appears on the directory)
# - DISK$N (The path of the disk file)
detect_machine() {
    export CHANNEL="20.03"

    source "${SCRIPT_PATH}/env"
}
