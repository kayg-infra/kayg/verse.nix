# The function first checks the existence of the above two channels. If they do
# not exist, it adds the nixos-$VERSION and nixos-unstable channels system-wide
# and refreshes the cache to reflect the channel additions.
#
# Next it checks for the existence of home-manager in the user-profile. The
# channel addition and updation is performed if they do not exist. Home-manager
# requires logging out and back in in the midst of installation so if the
# installation command fails (which it will in the first attempt, as the $PATH
# isn't updated until the user has logged in again), the script outputs a
# message and exits. On the next run, however, home-manager will definitely be
# installed.
#
# The directory ~/.config/nixpkgs is crucial for home-manager as both home.nix
# and config.nix are placed (in my case, symlinked) in that directory. It is
# created if it doesn't already exist. Next the symlinks' existence is checked
# and they are created if they do not exist.
#
# Finally, home-manager switch is run which fetches the packages defined in
# home.packages from the nixos-unstable channel.
#
# As I do not store hardware-configuration.nix in this repository, it is
# necessary for me to either copy or symlink the file to the path where
# configuration.nix exists. Since configuration.nix is already symlinked to
# /etc/nixos/configuration.nix, a symlink of
# /etc/nixos/hardware-configuration.nix to the current repo feels very
# convinient.
#
# - dirname: yields the pathname from the invocation. For example, if the
#   invocation is /tmp/abc.sh, the result would be /tmp. However, if the
#   invocation is ./tmp/abc.sh, the output is ./tmp instead of the absolute
#   path; which is where realpath comes into play.
#  
# - realpath: returns the absolute path of the script and does not resolve
#   symlinks when -s is specified. The trick here is to use dirname and realpath
#   in conjunction to get the absolute path of the script excluding the script
#   name.
#
# - All output from the conditionals has been discarded because they provide no
#   meaningful value other than mitigating the abstraction.
#
# - git has to be uninstalled from the system profile because I have it
#   configured with home-manager which often installs the same version of git
#   and results in conflict, therefore manual intervention.
add_nixos_channels() {
    printf "Adding NixOS ${CHANNEL} channel."
    sudo nix-channel --add https://nixos.org/channels/nixos-${CHANNEL} nixos && \
        status

    # add home manager channel and optionally the unstable channel
    if [[ "${CHANNEL}" == "unstable" ]]; then
        printf "Adding Home-Manager unstable channel."
        sudo nix-channel --add https://github.com/rycee/home-manager/archive/master.tar.gz home-manager && \
            status
    else
        printf "Adding NixOS unstable channel."
        sudo nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable && \
            status
        printf "Adding Home Manager channel."
        sudo nix-channel --add https://github.com/rycee/home-manager/archive/release-${CHANNEL}.tar.gz home-manager && \
            status
    fi

    printf "Refresh channel metadata."
    sudo nix-channel --update && \
        status
}

setup_home() {
    # Create dir if it does not exist
    dir="${HOME}/.config/nixpkgs"
    mkdir -p "${dir}"

    printf "Linking home configuration."
    ln -sf "${SCRIPT_PATH}/machines/${MACHINE}/home.nix" "${dir}" && \
        ln -sf "${SCRIPT_PATH}/modules/nix/config.nix" "${dir}" && \
        status

    printf "Installing home manager.\n"
    set +u
    if ! command -v home-manager > /dev/null 2>&1; then
        if ! nix-shell '<home-manager>' -A install; then
            printf "\nPlease relogin and run the script again.\n"
            exit
        fi
    else
        printf "Home manager already installed. Switching state."
        home-manager switch
        set -u
    fi
}

setup_system() {
    printf "Linking system configuration."
    ln -sf "/etc/nixos/hardware-configuration.nix" "${SCRIPT_PATH}/machines/${MACHINE}" && \
        sudo ln -sf "${SCRIPT_PATH}/machines/${MACHINE}/configuration.nix" "/etc/nixos" && \
        status
}
