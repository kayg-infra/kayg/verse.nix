# Encrypt drives before making a filesystem. Format the partition with LUKS2
# with the passphrase from STDIN and then map the device to a name provided in
# the script according to the machine name.
mkcrypt() {
    printf "Encrypting the root drive.\n"
    cryptsetup --batch-mode --verify-passphrase luksFormat "${1}"

    printf "Decrypting the encrypted drive for use.\n"
    cryptsetup open "${1}" "${2}"
}

# Generate an SSH host key for Dropbear for openssh client to stop freaking out
# everytime it connects. Dropbear does not support ED25519.
mk_ssh_key() {
    # Create ECDSA host key as I do not use RSA or DSS
    # nix-shell has a few unset variables
    set +u
    printf "Generating Dropbear SSH key.\n"
    mkdir -p /mnt/etc/dropbear && \
        nix-shell -p dropbear --command "dropbearkey -t ecdsa -s 521 -f /mnt/etc/dropbear/host-key" && \
        status
    set -u
}
