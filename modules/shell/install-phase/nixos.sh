# Once the configuration has been generated at /etc/nixos, they are replaced by
# symbolically linking my custom configuration and copying the generated
# hardware configuration.
#
# The function takes an optional argument; the primary channel to be used by the
# machine. If no such argument is provided, unstable is assumed. The
# home-manager channel is then added since I want all setup to be done at
# deployment time and then all channels are updated.
#
# Since I would like to avoid any license drama with non-free software such as
# nVidia, I allow installation of non-free software by setting
# NIXPKGS_ALLOW_UNFREE to a value of 1. Since I want an uninterrupted
# installation, I disable the root password prompt with --no-root-passwd. If the
# installation is successful, the pools are exported with a message containing
# instructions to import it again. If the installation fails for whatever
# reason, a prompt is displayed for retrial.
gen_nixos_config() {
    # Generate automatic configuration
    printf "Creating NixOS hardware configuration."
    run nixos-generate-config --no-filesystems --root /mnt

    # Replace configuration by the modified one
    printf "Adding NixOS system configuration."
    ln -sf "${SCRIPT_PATH}/machines/${MACHINE}/configuration.nix" "/mnt/etc/nixos" && \
        cp "/mnt/etc/nixos/hardware-configuration.nix" "${SCRIPT_PATH}/machines/${MACHINE}" && \
        status
}

add_nixos_channels() {
    printf "Adding NixOS channels."
    run nix-channel --add https://nixos.org/channels/nixos-${CHANNEL} nixos

    # add home manager channel and optionally the unstable channel
    if [[ "${CHANNEL}" == "unstable" ]]; then
        printf "Detected channel: unstable. Adding home-manager unstable channel."
        run nix-channel --add https://github.com/rycee/home-manager/archive/master.tar.gz home-manager
    else
        printf "Detected channel: stable. Adding home-manager stable and nixos-unstable channel."
        nix-channel --add https://nixos.org/channels/nixos-unstable nixos-unstable && \
            nix-channel --add https://github.com/rycee/home-manager/archive/release-${CHANNEL}.tar.gz home-manager && \
            status
    fi

    printf "Refresh channel metadata."
    run nix-channel --update
}

install_nixos() {
    # allow unfree packages during installation
    export NIXPKGS_ALLOW_UNFREE=1

    printf "Installing NixOS on ${DISPLAY}.\n"

    # nixos-install has some intentionally empty variables and nounset throws
    # unbound variable errors. It can be temporarily disabled.
    set +u
    while true; do
        if nixos-install --no-root-passwd --root /mnt; then
            # Set nounset again
            set -u
            # Wait for vdevs to settle
            sleep 1

            # Unmount everything
            umount -R /mnt
            swapoff -a

            break
        else
            printf "\nThe installer seems to have exited with an error. Do you want to restart? (y/n): "
            read -r prompt

            while true; do
                case "${prompt}" in
                    "Y"|"y")
                        break
                        ;;
                    "N"|"n")
                        exit 1
                        ;;
                    *)
                        printf "\nPlease enter y or n: "
                        ;;
                esac
            done
        fi
    done
}
