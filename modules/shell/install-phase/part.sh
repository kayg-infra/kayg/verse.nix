# A discard operation or a TRIM operation is the operating system informing the
# storage device's firmware that blocks of data are no longer in use and can be
# wiped internally.
#
# If the storage device is not mechanical -- is a Solid State Drive then all
# blocks are discarded. If the storage device is not a SSD, its partition table
# data is erased. This is due to the fact that SSDs work very different in
# comparision to HDDs.
wipe_disk() {
    printf "Wiping disk. "
    if [[ "${1}" == "--ssd" ]]; then
        printf "Flash storage detected."
        run blkdiscard "${2}"
    else
        printf "Rotational storage detected."
        run sgdisk --zap-all "${1}"
    fi
}


# At first, I was confused as the manpage of =sgdisk= simply mentions that the
# flag --mbrtogpt converts a BSD label or MBR labeled partition table to GPT.
# What it fails to mention is that even if such a table does not exist, a GPT is
# created.
#
# Prior to the creation, a wipe of partition data is performed (redundant, I
# realize). Since servers are booted in *legacy* mode, a BIOS partition is
# created to store GRUB's binary. While with home systems, the preferred boot
# mode is UEFI so a small EFI System Partition is created to store the
# bootloader. Both are of the same size: 2 MiB.
#
# A partition for Nix to store generations, kernel and initramfs is created as
# GRUB is incapable yet to probe into an encrypted ZFS pool.
#
# A partition for ZFS pool and another for swapping is created. I would normally
# prefer to put SWAP on a ZFS volume but at the time of writing this, there is
# an unresolved deadlock issue with ZFS (one that I have experienced on live
# servers) that occurs irrespective of the size of your swap zvol.
#
# Lastly, a small delay is introduced for the partitions to go live.
mk_part_table() {
    # $1 -> Device name (/dev/sda, /dev/nvme0n1, etc)
    printf "Creating a GUID partition table."
    run sgdisk --clear --mbrtogpt "${1}"
}

mk_bios_part() {
    # 1 -> partition number
    # 2 -> size of the partition
    # 3 -> device file
    # Create a 2 MiB ESP or BIOS partition
    printf "Creating partition for BIOS."
    run sgdisk --new="${1}":0:+"${2}" --typecode="${1}":ef02 --change-name="${1}":"BIOS\ Drive" "${3}"
}

mk_boot_part() {
    # 1 -> partition number
    # 2 -> size of the partition
    # 3 -> device file
    # Create partition for grub and initrd storage
    printf "Creating partition for bootloader."
    run sgdisk --new="${1}":0:+"${2}" --typecode="${1}":8300 --change-name="${1}":"Boot\ Drive" "${3}"
}

mk_root_part() {
    # 1 -> partition number
    # 2 -> device file
    printf "Creating root partition."
    run sgdisk --new="${1}":0:0 --typecode="${1}":bf01 --change-name="${1}":"Root\ Drive" "${2}"

    # Wait for partitions to populate
    sleep 1
}

mk_raid_part() {
    # 1 -> partition number
    # 2 -> device file
    printf "Creating RAID partition."
    run sgdisk --new="${1}":0:0 --typecode="${1}":fd00 --change-name="${1}":"Root\ Drive" "${2}"

    # Wait for partitions to populate
    sleep 1
}
