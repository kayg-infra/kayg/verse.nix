mk_raid() {
    printf 'Creating MDADM RAID with\n  level: %s\n  name: %s\n' "${1}" "${2}"
    mdadm \
      --create \
      --homehost "${MACHINE}" \
      --level="${1}" \
      --name "${2}" \
      --raid-devices="${3}" \
      /dev/md/"${2}" \
      "${@:4}"
}

stop_raid() {
    printf 'Stopping all RAID arrays'
    run mdadm --stop --scan
}

rm_raid() {
    printf 'Erasing RAID superblock for: %s' "${1}"
    if [[ -e "${1}" ]]; then
        run mdadm --zero-superblock "${1}"
    else
        printf '\n'
    fi
}
