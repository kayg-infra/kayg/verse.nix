mk_swap_part() {
    # 1 -> partition number
    # 2 -> size of the partition
    # 3 -> device file
    printf "Creating partition for SWAP."
    run sgdisk --new="${1}":0:+"${2}" --typecode="${1}":8200 --change-name="${1}":"Swap\ Drive" "${3}"
}

# A SWAP filesystem is created with the label "Swap Drive". It is required in
# cases of high memory load, some of the information (according to LRU, the
# least recently used unit) is written from the RAM to disk (the IO speed of
# that disk determines the delay) and is written back when that unit is used
# again.
#
# The tendency of the kernel to SWAP is determined by a sysctl parameter:
# vm.swappiness
mk_swap_file() {
    printf "Creating swap file: ${2} of size: ${1}"
    mkdir -p "$(dirname ${2})"
    run dd if=/dev/zero of="${2}" bs=1G count="${1}" oflag=sync
    printf "Setting correct permissions on swap file."
    run chmod 600 "${2}"
}

mk_swap_fs() {
    printf "Formatting swap: ${2} with LABEL: ${1}."
    run mkswap -fL "${1}" "${2}"
}

activate_swap() {
    printf "Activating swap partition."
    run swapon -p 0 "${1}"
}

stop_swap() {
    printf 'Stopping all SWAP devices'
    run swapoff -a
}
