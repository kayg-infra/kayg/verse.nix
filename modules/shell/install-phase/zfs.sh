# This function checks if there are any existing pools. If any exist, they are
# forcibly imported (to /mnt) and then destroyed. The reason behind this is that
# corrupted or otherwise pools fail if imported normally.
#
# If no pools are found, a message is printed and the function exits.
destroy_pools() {
    # Parse available pools
    if [[ -n "$(zpool list -H)" ]]; then
        # Destroy them
        pool="$(zpool import | grep pool: | awk '{print $2}'; echo -n)"
        printf "Found ${pool}. Importing it."
        run zpool import -fR /mnt "${pool}"

        printf "Destroying ${pool}."
        run zpool destroy "${pool}"
    else
        printf "No pools found.\n"
    fi
}

# In this day and time, living without encryption is futile. All data is
# essential and identifiable. Mobile Phones are already encrypted by the OEM;
# there is little reason to go rogue with desktop or server devices. But to say
# and to do are things of opposite nature. I'm not a fan of Linux's tradtional
# RAID or encryption where each layer lives independent and oblivous of the
# other's existence. Luckily, ZFS is both a filesystem as well as a fully
# featured volume manager. It is my preferred filesystem on distributions like
# NixOS where the pain of license incompatibility is abstracted away. Since ZFS
# recently got encryption in mainline releases, there are no excuses left for
# me.
#
# The pool name is read through a variable. I set it usually the same as the
# hostname of the machine. The nounset bit is disabled to facililate creation of
# both single and multi-device pools. A set of features and properties are
# enabled for performance and preference.
#
# - The feature ashift is enabled and is set to 12 (2 ^ 12 = 4096, default value
#   is 9). It sets the block size of the filesystem to 4 KiB to match the
#   drive's sector size. Modern disks use Advance Format which means each sector
#   is 4096 byte long instead of the usual 512 bytes. However, leaving it to
#   auto-detection would result in ashift being set to 9 as most disks emulate a
#   512 byte sector size to maintain compatibility with all Operating Systems.
#   Here
#   [https://stackoverflow.com/questions/12345804/difference-between-blocks-and-sectors]
#   is a StackOverflow answer explaining disk sectors and blocks.
#
# - The property acltype is set to posixacl to enable the storage of Access
#   Control Lists (ACLs) as extended attributes. This option is specific to
#   Linux and will not work on other kernels. An indepth explanation of ACLs can
#   be found here [https://www.linux.com/news/posix-acls-linux].
#
# - The property atime is set to off as updating the access time everytime a
#   file is read is a costly, secondary operation. The likes of using it are
#   minimal to non-existent since it makes more sense to rely on mtime instead.
#
# - The property canmount is set to off. It describes the mountablity of a
#   certain dataset. For example, if a parent dataset solely exists for
#   structure and its child datasets are to be mounted, setting the canmount
#   property ensures that the parent dataset is never / cannot be mounted.
#
# - The property compression is set to lz4. LZ4 is a very cheap and fast
#   compression method that offers sizeable gains in storage for insignificant
#   loss in performance. Disk space is cheap, save as much as possible.
#
# - The property dedup is set to off. ZFS' deduplication algorithm requires a
#   lot of RAM with a large hit in IO speeds. Normally, ZFS uses as much RAM as
#   possible but does not need it. But with deduplication on, it becomes a
#   requirement. I find compression to be a far better compromise.
#
# - The property dnodesize is set to auto. ZFS manual recommends this if the
#   workload makes use of extended attributes. I have no idea what this does.
#
# - The property encryption is set to aes-256-gcm. The preferred cipher suite
#   with most pools seems to be AES-256-GCM. ZFS defaults to CCM. However, a
#   good explanation of why GCM is superior can be found here
#   [https://crypto.stackexchange.com/questions/6842/how-to-choose-between-aes-ccm-and-aes-gcm-for-storage-volume-encryption].
#
# - The properties keyformat and keylocation is set to passphrase and prompt
#   respectively. All data needs to be encrypted with a password or with a file
#   containing that password. I want ZFS to ask for the password during the
#   creation of the pool.
#
# - The property xattr is set sa. Extended attributes are supported by ZFS
#   either directory based or system attribute based. Storing system attributes
#   as extended attributes significantly decreases the amount of disk IO
#   required. Since ACLs are stored as extended attributes, a value of sa which
#   enabled system attribute based extended attributes is preferred over the
#   option on which uses directory based extended attributes.
#
# - The flag -m (or mountpoint) is set to none as I do not want to mount the
#   pool (or root dataset).
#
# A delay of 1 second is introduced for the kernel to read the new pool.
# Afterwards the pool is exported for a later reimport.
mk_root_fs() {
    # input pool name
    printf "Enter new pool name: "
    read -r newpool

    set +u
    # create pool
    # -O filesystem property
    # -m mountpoint
    printf "Formatting root partition."

    # if --mirror is passed, the argument list is split from the second
    # argument. That makes $1 the option and $2, 3, 4... the disk files to span
    # the pool on.
    if [[ "${1}" == "--mirror" ]]; then
        run zpool create -fo ashift=12 \
            -O acltype=posixacl \
            -O atime=off \
            -O canmount=off \
            -O compression=lz4 \
            -O dedup=off \
            -O dnodesize=auto \
            -O xattr=sa \
            -m none \
            "${newpool}" \
            mirror \
            ${@:2}
    else
        run zpool create -fo ashift=12 \
            -O acltype=posixacl \
            -O atime=off \
            -O canmount=off \
            -O compression=lz4 \
            -O dedup=off \
            -O dnodesize=auto \
            -O xattr=sa \
            -m none \
            "${newpool}" \
            ${@}
    fi
    set -u

    # Wait for pool to populate
    sleep 1

    printf "Exporting ${newpool}."
    run zpool export "${newpool}"
}

# The name of the pool is passed as an argument to the function and thereafter,
# the pool is imported at /mnt. The folks at #nixos freenode convinced me that
# the more Nix knows about my system, the better. So, instead of letting ZFS
# handle all datasets for me, the system datasets are of legacy nature (that is,
# they are mounted through /etc/fstab) and other datasets are handled by ZFS.
mk_zfs_datasets() {
    # fetch poolname
    poolname="${1}"

    # Import pool to /mnt
    printf "Importing pool to /mnt."
    run zpool import -d /dev/disk/by-id -R /mnt "${poolname}"

    printf "Creating system datasets."
    zfs create -o mountpoint=none "${poolname}"/rootfs && \
        zfs create -o mountpoint=legacy "${poolname}"/rootfs/default && \
        status

    printf "Creating docker dataset."
    run zfs create -o mountpoint=legacy "${poolname}"/docker
}

mount_zfs_datasets() {
    printf "Mounting root dataset."
    run mount -t zfs "${1}"/rootfs/default /mnt

    printf "Mounting docker dataset."
    mkdir -p /mnt/var/lib/docker && \
        mount -t zfs "${1}"/docker /mnt/var/lib/docker && \
        status
}

# ZFS has TRIM capabilities now which is required to free up unused blocks on a
# SSD at the firmware level.
enable_auto_trim() {
    printf "Turning on ZFS auto trim for SSDs."
    run zpool set autotrim=on "${1}"
}

unmount_zfs_pool() {
    # Export pool if installation is successful
    zfs unmount -a
    zpool export "$(zpool list | cut -d ' ' -f1 | sed -ne '2p')"

    printf "\nPool exported. Run the following command if you wish to import it: zpool import -d /dev/disk/by-id -R /mnt $(zpool list | cut -d ' ' -f1 | sed -ne '2p')\n"
}
