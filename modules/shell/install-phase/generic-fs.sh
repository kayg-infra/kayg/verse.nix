# Out of the 4 partitions created in the last step, three partitions are
# formatted with a filesystem.
mk_boot_fs() {
    printf "Formatting boot partition."
    run mkfs.ext4 -L "${1}" "${2}"
}

# Datasets of legacy nature are mounted manually. If it's a laptop, the ESP is
# mounted at /boot/efi (the noatime property explained earlier applies here too)
# and if it's a server, the docker dataset is mounted at /var/lib/docker.
mount_boot_fs() {
    # $1 -> Boot Drive
    printf "Mounting boot partition."
    mkdir -p /mnt/boot && \
        mount -o defaults,noatime,nobarrier,commit=60 "${1}" /mnt/boot && \
        status
}
