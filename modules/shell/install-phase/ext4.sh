mk_root_fs() {
    if [[ "${1}" == '--raid' || "${1}" == '-r' ]]; then
        printf 'Creating ext4 filesystem on disk: %s' "/dev/md/${2}"
        run mkfs.ext4 \
            -E lazy_itable_init=0,lazy_journal_init=0,stride="${stride}",stripe-width="${stripe_width}" \
            -m 0.001 \
            -L "${2}" \
            -T huge \
            /dev/md/"${2}"
    else
        printf 'Creating ext4 filesystem on disk: %s' "${1}"
        run mkfs.ext4 \
            -E lazy_itable_init=0,lazy_journal_init=0 \
            -m 0.01 \
            -L "${1}" \
            -T huge \
            "${1}"
    fi

    sleep 1
}

mount_root_fs() {
    printf 'Mounting root filesystem at /mnt'
    run mount \
        -t ext4 \
        -o defaults,noatime,nobarrier,commit=60 \
        "${1}" \
        "/mnt"
}
