{ config, lib, pkgs, ... }:

{
  programs = {
    # Let Home Manager install and manage itself.
    home-manager.enable = true;

    # Enable smart cat clone
    bat = {
      enable = true;
      config = {
        pager = "less -FR";
        theme = "TwoDark";
      };
    };

    # Enable command suggestions
    command-not-found.enable = true;

    # Enable htop
    htop = {
      enable = true;
      colorScheme = 5;
      delay = 10;
      detailedCpuTime = true;
      highlightBaseName = true;
      highlightMegabytes = true;
      highlightThreads = true;
    };

    /* Enable Neovim and alias both vi and vim to nvim. */
    neovim = {
      enable = true;
      viAlias = true;
      vimAlias = true;
    };

    # Enable TMUX
    tmux = {
      enable = true;
      baseIndex = 1;
      clock24 = true;
      historyLimit = 100000;
      newSession = true;
      keyMode = "vi";
      terminal = "xterm-256color";
      shortcut = "a";
    };

    zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      oh-my-zsh = {
        enable = true;
        plugins = [ "git" ];
        theme = "refined";
      };
      initExtra = "DISABLE_AUTO_TITLE=true";
    };
  };
}
