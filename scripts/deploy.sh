#!/usr/bin/env bash

SCRIPT_PATH=$(dirname $(realpath --no-symlinks "$0"))
export SCRIPT_PATH

main() {
    # Import essential modules
    # Make all variables and functions persist through child shells
    set -a
    source "${SCRIPT_PATH}/modules/shell/helpers/generic-utils.sh"
    set +a
    source "${SCRIPT_PATH}/modules/shell/detect.sh"

    # detect machine and installation mode
    detect_machine
    detect_mode

    if [[ "${MODE}" == "PRE" ]]; then
        # run the install script
        bash "${SCRIPT_PATH}/machines/${MACHINE}/install.sh"
    elif [[ "${MODE}" == "POST" ]]; then
        # run the setup script
        bash "${SCRIPT_PATH}/scripts/setup.sh"
    fi
}

main
