#!/usr/bin/env bash

main() {
    # Import essential modules
    source "${SCRIPT_PATH}/modules/shell/setup-phase/nixos.sh"

    add_nixos_channels
    setup_system
    setup_home
}

main
